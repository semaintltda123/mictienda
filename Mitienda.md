Schema mydb ;

USE `mydb` ;

-- Table `mydb`.`p_categorias`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`p_categorias` (
  `idp_categorias` INT NOT NULL,
  `Descripcion` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idp_categorias`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Productos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Productos` (
  `idProductos` INT NOT NULL,
  `Nombre_productos` VARCHAR(45) NOT NULL,
  `Descripcion_productos` VARCHAR(45) NOT NULL,
  `Foto` VARCHAR(45) NULL,
  `precio` DECIMAL NOT NULL,
  `p_categorias_idp_categorias` INT NOT NULL,
  PRIMARY KEY (`idProductos`, `p_categorias_idp_categorias`),
  INDEX `fk_Productos_p_categorias_idx` (`p_categorias_idp_categorias` ASC) VISIBLE,
  CONSTRAINT `fk_Productos_p_categorias`
    FOREIGN KEY (`p_categorias_idp_categorias`)
    REFERENCES `mydb`.`p_categorias` (`idp_categorias`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Rol`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Rol` (
  `idRol` INT NOT NULL,
  `nombre_rol` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idRol`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Usuario` (
  `idUsuario` INT NOT NULL,
  `identificacion` VARCHAR(45) NOT NULL,
  `nombre_completo` VARCHAR(45) NOT NULL,
  `correo_electronico` VARCHAR(45) NOT NULL,
  `direccion` VARCHAR(45) NOT NULL,
  `telefono` INT(10) NOT NULL,
  `contraseña` VARCHAR(45) NOT NULL,
  `Rol_idRol` INT NOT NULL,
  PRIMARY KEY (`idUsuario`, `Rol_idRol`),
  INDEX `fk_Usuario_Rol1_idx` (`Rol_idRol` ASC) VISIBLE,
  CONSTRAINT `fk_Usuario_Rol1`
    FOREIGN KEY (`Rol_idRol`)
    REFERENCES `mydb`.`Rol` (`idRol`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Inventarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Inventarios` (
  `idInventario` INT NOT NULL,
  `Productos_idProductos` INT NOT NULL,
  `catidad` INT NOT NULL,
  PRIMARY KEY (`idInventario`, `Productos_idProductos`),
  INDEX `fk_Inventario_Productos1_idx` (`Productos_idProductos` ASC) VISIBLE,
  CONSTRAINT `fk_Inventario_Productos1`
    FOREIGN KEY (`Productos_idProductos`)
    REFERENCES `mydb`.`Productos` (`idProductos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Compra`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Compra` (
  `idCompra` INT NOT NULL,
  `fecha` DATE NOT NULL,
  `` VARCHAR(45) NULL,
  `Usuario_idtendero` INT NOT NULL,
  `Usuario_idcomprador` INT NOT NULL,
  PRIMARY KEY (`idCompra`, `Usuario_idtendero`, `Usuario_idcomprador`),
  INDEX `fk_Compra_Usuario1_idx` (`Usuario_idtendero` ASC) VISIBLE,
  INDEX `fk_Compra_Usuario2_idx` (`Usuario_idcomprador` ASC) VISIBLE,
  CONSTRAINT `fk_Compra_Usuario1`
    FOREIGN KEY (`Usuario_idtendero`)
    REFERENCES `mydb`.`Usuario` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Compra_Usuario2`
    FOREIGN KEY (`Usuario_idcomprador`)
    REFERENCES `mydb`.`Usuario` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Detalle_compra`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Detalle_compra` (
  `Productos_idProductos` INT NOT NULL,
  `Compra_idCompra` INT NOT NULL,
  `cantidad_productos` INT NOT NULL,
  PRIMARY KEY (`Productos_idProductos`, `Compra_idCompra`),
  INDEX `fk_Productos_has_Compra_Compra1_idx` (`Compra_idCompra` ASC) VISIBLE,
  INDEX `fk_Productos_has_Compra_Productos1_idx` (`Productos_idProductos` ASC) VISIBLE,
  CONSTRAINT `fk_Productos_has_Compra_Productos1`
    FOREIGN KEY (`Productos_idProductos`)
    REFERENCES `mydb`.`Productos` (`idProductos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Productos_has_Compra_Compra1`
    FOREIGN KEY (`Compra_idCompra`)
    REFERENCES `mydb`.`Compra` (`idCompra`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
