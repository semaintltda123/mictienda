/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minticgrupo2.mitienda;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import persistencia.ConexionBD;

/**
 *
 * @author krnmt
 */
public class Productos {
    
    private int idProductos;
    private String nombre_productos;
    private String descripcion_productos;
    private String foto;
    private String precio;

    public Productos() {
    }
    
    public Productos getContacto(int idProductos) throws SQLException {
        this.idProductos = idProductos;
        return this.getContacto();
    }

    public int getIdProductos() {
        return idProductos;
    }

    public void setIdProductos(int idProductos) {
        this.idProductos = idProductos;
    }

    public String getNombre_producto() {
        return nombre_productos;
    }

    public void setNombre_producto(String nombre_productos) {
        this.nombre_productos = nombre_productos;
    }

    public String getDescripcion_producto() {
        return descripcion_productos;
    }

    public void setDescripcion_producto(String descripcion_productos) {
        this.descripcion_productos = descripcion_productos;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public boolean guardarProducto() {
        ConexionBD conexion = new ConexionBD();
        String sentencia = "INSERT INTO productos(idProductos, nombre_productos, descripcion_productos, foto, precio) "
                + " VALUES ( '" + this.idProductos + "','" + this.nombre_productos + "',"
                + "'" + this.descripcion_productos + "','" + this.foto + "','" + this.precio + "');  ";
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.insertarBD(sentencia)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }

    public boolean borrarProducto(int idProductos) {
        String Sentencia = "DELETE FROM `productos` WHERE `idProductos`='" + idProductos + "'";
        ConexionBD conexion = new ConexionBD();
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(Sentencia)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }

    public boolean actualizarProducto() {
        ConexionBD conexion = new ConexionBD();
        String Sentencia = "UPDATE `contactos` SET nombre='" + this.nombre_productos + "',descripcion='" + this.descripcion_productos + "',foto='" + this.foto
                + "',precio='" + this.precio + ";";
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(Sentencia)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }

    public List<Productos> listarProductos() throws SQLException {
        ConexionBD conexion = new ConexionBD();
        List<Productos> listaContactos = new ArrayList<>();
        String sql = "select * from productos order by idProductos asc";
        ResultSet rs = conexion.consultarBD(sql);
        Productos p;
        while (rs.next()) {
            p = new Productos();
            p.setIdProductos(rs.getInt("idProductos"));
            p.setNombre_producto(rs.getString("nombre_producto"));
            p.setDescripcion_producto(rs.getString("descriocion_producto"));
            p.setFoto(rs.getString("foto"));
            p.setPrecio(rs.getString("precio"));
            listaContactos.add(p);

        }
        conexion.cerrarConexion();
        return listaContactos;
    }

    public Productos getProducto() throws SQLException {
        ConexionBD conexion = new ConexionBD();
        String sql = "select * from contactos where identificacion='" + this.idProductos + "'";
        ResultSet rs = conexion.consultarBD(sql);
        if (rs.next()) {
            this.idProductos = rs.getInt("idProductos");
            this.nombre_productos = rs.getString("nombre_productos");
            this.descripcion_productos = rs.getString("descripcion_productos");
            this.foto = rs.getString("foto");
            this.precio = rs.getString("precio");         
            conexion.cerrarConexion();
            return this;

        } else {
            conexion.cerrarConexion();
            return null;
        }

    }
    
    @Override
    public String toString() {
        return "Productos{" + "idUsuario=" + idProductos + ", nombre_producto=" + nombre_productos + ", descripcion_producto=" + descripcion_productos + ", foto=" + foto + ", precio=" + precio + '}';
    }
    
    
}
